CREATE TABLE `users` (
  `user_id` int(11) NOT NULL auto_increment,
  `handle` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_salt` varchar(32) default NULL,
  `last_login` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM COMMENT = 'Table stores all user login credentials.' DEFAULT CHARSET=latin1 ;

CREATE TABLE  `user_profile` (
 `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `user_id` INT NOT NULL ,
 `firstname` VARCHAR( 100 ) NOT NULL ,
 `lastname` VARCHAR( 100 ) NOT NULL
) ENGINE = MYISAM COMMENT =  'Table stores all user relevant informations.' DEFAULT CHARSET=latin1;

CREATE TABLE  `interest` (
 `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `parent_id` INT NULL ,
 `label` VARCHAR( 255 ) NOT NULL ,
 `description` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM COMMENT =  'Table stores all interests with short description.' DEFAULT CHARSET=latin1;

CREATE TABLE  `provider` (
 `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `label` VARCHAR( 100 ) NOT NULL ,
 `description` TEXT NOT NULL ,
 `logo` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM COMMENT =  'This table stores all basic provider informations.' DEFAULT CHARSET=latin1;

CREATE TABLE  `zf_stopandgo`.`theme` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
 `label` VARCHAR( 100 ) COLLATE latin1_general_ci NOT NULL ,
 `description` TEXT COLLATE latin1_general_ci NOT NULL ,
 `logo` VARCHAR( 255 ) COLLATE latin1_general_ci NOT NULL ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = latin1 COLLATE = latin1_general_ci COMMENT =  'This table stores all basic theme informations.';