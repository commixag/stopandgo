<?php

class Zend_View_Helper_AlphasortedBoxes
{
     public $view;
     
     private $url;
     private $html;
    
     function AlphasortedBoxes($data, $url = array('controller'=>'page','page'=>'info')) 
     {
        $this->url = $url;
        
        $r = 1;
			  $curCharacter = '';
			  $list = array();
			  
			  // generate sorted lists      	
			  foreach($data as $key=>$item)
				{
				   // check first character
			      $firstChar = strtolower(substr($item['label'],0,1));
				    if($curCharacter != $firstChar){			    	        		            		  	        
			          if($curCharacter != '')
			              $list[$curCharacter] .= "<a href=\"" . $this->view->url($this->url,'default',true) . 
						                                "\">... mehr</a></li>";
				        $curCharacter = $firstChar;          
			          $changedChar = true;
			      }	    		            
					  
						if($changedChar == true) $list[$curCharacter] .= "<li class=\"first\">";
						else $list[$curCharacter] .= "<li>";
						
						$this->url['id'] = $item['id'];
						$list[$curCharacter] .= "<a href=\"" . $this->view->url($this->url,'default',true) . 
						                        "\">" . $item['label'] . "</a></li>";
						$changedChar = false;	  	    
				}
				
				$r = 1;	
				foreach(array_keys($list) as $char)
				{
			      if($r == 1){
			          $cbs .= "<div class=\"teaser-box-list-row\">";		            
			      }
			      $cbs .= "<div class=\"teaser-box-list brown\">";
			      $cbs .= "<div class=\"tb-content\">";					
			      $cbs .= "	<h3>" . strtoupper($char) . "</h3><ul>";
			      $cbs .= $list[$char];					
			      $cbs .= "</div></div>";
			
			      if($r == 4){
			           $cbs .= "</div>";
			           $r = 0;		            
			      }
				    $r++;    
				}
				
				//if($r == 5 || $r < 4) $cbs .= "</div>";
				
				return $cbs;
     }
     
     public function setView(Zend_View_Interface $view)
     {
        $this->view = $view;
     } 
}