<?php

class Zend_View_Helper_TeaserBoxenTabs 
{
     public $view;
     
     private $url;
     private $html;
    
     function TeaserBoxenTabs($tabs, $url = array('controller'=>'page','action'=>'publidetail')) 
     {
        $this->url = $url;
        // iterate all entries
        $t = 1;
        foreach($tabs as $tab)
        {
            $html .= $this->view->contentPane( 'tab'.$t, $this->getTeaserBoxes($tab['data'], $url),
                             array('region' => 'top','title'=>$tab['label']),
                             array('style' => 'background-color: white;'));
            $t++;            
        }
  
        return $html;
     }

    private function getTeaserBoxes($boxes)
    {
        $cbs = "";
        $r = 1;
		    foreach($boxes as $box)
		    {
		        if($r == 1){
		            $cbs .= "<div class=\"teaser-box-list-row\">";		            
		        }
		        $cbs .= "<div class=\"teaser-box-list brown\">";
			      $cbs .= "<div class=\"tb-content\">";		
			      $cbs .= "<a href=\"" . $this->view->url($box['url'],'default',true) . "\">";			
				    $cbs .= "<div class=\"preview-pic\" style=\"background-image: url('" . $box['pic'] . "');\"></div></a>";
				    $cbs .= "<h3>" . $box['title'] . "</h3>";
				    $cbs .= "<p>" . $box['descr'] . "</p>";
				    $cbs .= "<a href=\"" . $this->view->url($box['url'],'default',true) . "\">&raquo; anschauen</a>";			
						$cbs .= "</div></div>";
		        if($r == 4){
		            $cbs .= "</div>";		            
		        }
				    $r++;    
		    }
        return $cbs;
    }
     
     public function setView(Zend_View_Interface $view)
     {
        $this->view = $view;
     } 
}