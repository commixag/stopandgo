<?php

class ThemeController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $mTheme = new Application_Model_Theme();
        $rows = $mTheme->getList();
        
        $this->view->themes = $rows->toArray(); 
        
        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );        
    }

    public function detailAction()
    {
    	  $mTheme = new Application_Model_Theme();
        $tabs = $mTheme->getSublist((int)$this->getRequest()->getParam('id'));
        
    		if(is_array($tabs)){
        	$this->view->themes = $tabs;
        }else{
        	$this->_redirect('page/empty');
        }

        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );        
    }
}

