<?php

class MaterialController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $mMaterial = new Application_Model_Material();
        $this->view->tabs = $mMaterial->getTabs();
        
        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );        
    }


}

