<?php
/**
 * Pseudo CMS controller 
 * Used to call predefined view scripts.
 * 
 */
class PageController extends Zend_Controller_Action
{
    
    public function init()
    {       
        /* Initialize action controller here */
    }

    public function __call($method, $args)
    {
        $sp = APPLICATION_PATH . '/views/scripts/page/';
        $vs = $this->getRequest()->action . '.phtml';
        if (file_exists( $sp . $vs)) {
	          $this->render($this->getRequest()->action);
	          
	          Zend_Registry::get('log')->log('Show page (' . $_SERVER['REQUEST_URI']  . ')', 
	          															 Zend_Log::INFO,
	          															 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          															 			 'controller'=>'page', 'action' => $this->getRequest()->action));
        } else {
            // TODO: Add 404 Error output.
						$this->getResponse()->setHttpResponseCode(404);
            throw new Exception('Invalid method');   
        }
    }
}



