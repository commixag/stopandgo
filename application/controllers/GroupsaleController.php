<?php

class GroupsaleController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $mGroupsale = new Application_Model_Groupsale();
        $this->view->tabs = $mGroupsale->getTabs();
        
        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );
    }

    public function detailAction()
    {
        $mGroupsale = new Application_Model_Groupsale();
        $this->view->details = $mGroupsale->getGroupsaleDetails(5);
        
        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );        
    }
}

