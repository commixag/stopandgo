<?php

class ProviderController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $mProvider = new Application_Model_Provider();
        $rows = $mProvider->getList();
        
        $this->view->providers = $rows->toArray(); 
        
        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );        
    }

    public function detailAction()
    {
        $mMaterial = new Application_Model_Material();
        
        $tabs = $mMaterial->getProviderMaterial((int)$this->getRequest()->getParam('id'));
        
        if(is_array($tabs)){
        	$this->view->tabs = $tabs;
        }else{
        	$this->_redirect('page/empty');
        }
        
        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );        
    }
}

