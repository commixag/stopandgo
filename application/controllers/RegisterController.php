<?php

class RegisterController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
         $registerForm = new Application_Form_Register($_POST);
	       $registerForm->setAction('/register/save');
	       $this->view->placeholder('registerForm')->append($registerForm);
	       
        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );	       
    }
    
    public function saveAction()
    {
        $regForm = new Application_Form_Register($_POST);
        
        /**
         * Autologin user
         * TODO: move authentication handling to a central location
         */ 
        if ($regForm->isValid($_POST)) {
            
            /**
		         * Save user in DB 
		         */			      
		        $mUser = new Application_Model_Users();
		        $mUserProfile = new Application_Model_UserProfile();		        		        
		        
		        $fValues = $regForm->getValues();
		        
	          $intUserId = $mUser->insert(array( 'handle'   => $fValues['email'],
	                                             'password' => md5($fValues['password']) ));
	                                
            $mUserProfile->insert(array( 'users_user_id'		=> $intUserId,
                                         'firstname'  => $fValues['firstname'],
	                                       'lastname'   => $fValues['lastname'],
	                                       'private_mail' => $fValues['email'] ));	                                
 
            $adapter = new Zend_Auth_Adapter_DbTable(
                $this->_getParam('db'),
                'users',
                'handle',
                'password',
                'MD5(?)'
                //'MD5(CONCAT(?, password_salt))'
                );
 
            $adapter->setIdentity($regForm->getValue('email'));
            $adapter->setCredential($regForm->getValue('password'));
 
            $result = Zend_Auth::getInstance()->authenticate($adapter);
            
            $mUser = new Application_Model_Users();
	      		$rUser = $mUser->fetchRow( $mUser->select()->where('handle LIKE "' . $fValues['email'] . '"') );
								
						Zend_Registry::get('sag')->curUser = $rUser->toArray();
 
            if ($result->isValid()) {
                $this->_helper->FlashMessenger('Erfolgreich angemeldet');
                $this->_redirect('/page/checkmail');
            }else{
                $this->_helper->FlashMessenger('Login ging schief.');   
                $this->_redirect('/register');
            }            
        } else {
            $this->_redirect('/register');
        }          

        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );        
    }

    private function sendRegisterMail()
    {
	      $config = $this->getInvokeArg('bootstrap')->getOptions();
	      
	      // setup Zend_Mail_Transport object
	      $transport = new Zend_Mail_Transport_Smtp($config['resources']['mail']['server'], $config['resources']['mail']);
	      
	      Zend_Mail::setDefaultFrom('info@stopandgo.com', 'Stop and Go');
        Zend_Mail::setDefaultReplyTo('info@stopandgo.com','Stop and Go');
	      
	      // setup mail message
	      $mail = new Zend_Mail();
        $mail->addTo('dresch@unknown.de', 'Dirk Dresch');
        // TODO: move to application.ini
        $mail->setSubject('stopandgo Registrierung');
        $mail->setBodyText('Vielen Dank für die Registrierung. Bitte bestätigen Sie den diese Mail Adresse durch klick auf den Link.');
        $mail->setBodyHtml('Vielen Dank für die Registrierung. Bitte bestätigen Sie den diese Mail Adresse durch klick auf den Link.');
        $mail->setFrom('info@stopandgo.ch');
	      $mail->send($transport);
	      
	      // Reset defaults
	      Zend_Mail::clearDefaultFrom();
        Zend_Mail::clearDefaultReplyTo();
    }
}



