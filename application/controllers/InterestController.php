<?php

class InterestController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function listAction()
    {
        $this->view->addHelperPath('SZend/Dojo/View/Helper/', 'SZend_Dojo_View_Helper');
    }
    
    public function recordsAction()
    {
        $mInterests = new Application_Model_Interests();
        $data= $mInterests->getInterestsList();
        
        $dojoData= new Zend_Dojo_Data('id',$data,'id');
        echo $dojoData->toJson();
        exit;
    }
    
    public function addAction()
    {
        // get parent interests for ComboBox
	      $mInterests = new Application_Model_Interests();
	      $formData['parent_ids'] = $mInterests->fetchParentInterests();
        
	      $options['noValue'] = "Keine Elternkategorie";
	      
        foreach($formData['parent_ids'] as $interest)
        {
            $options[$interest['id']] = $interest['label']; 
        }
        
        $interestForm = new Application_Form_Interest();
	      $interestForm->setAction('/interest/save');
	      $interestForm->getElement('parent_id')->addMultiOptions($options);
	      $interestForm->populate($formData);
	      $this->view->form = $interestForm;
    }
     
    public function saveAction()
    {
        $interestForm = new Application_Form_Interest($_POST);

        if($interestForm->isValid($_POST))
        {
            $values = $interestForm->getValues();
	          $mInterest = new Application_Model_Interests();
	          $values['parent_id'] = $mInterest->getIdFromLabel($values['parent_id']);
	          if(is_null($values['parent_id'])) unset($values['parent_id']);
            $mInterest->insert($values);
            
            $this->_redirect('/interest/add');
        } else {
            echo "sorry the given form data is not valid...";    
        }
        

        exit();
    }
}

