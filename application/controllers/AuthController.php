<?php

class AuthController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function loginAction()
    {
 
        $loginForm = new Application_Form_Login($_POST);        
 
        if ($loginForm->isValid($_POST)) {
 
            $adapter = new Zend_Auth_Adapter_DbTable(
                $this->_getParam('db'),
                'users',
                'handle',
                'password',
                'MD5(?)'
                //'MD5(CONCAT(?, password_salt))'
                );
 
            $adapter->setIdentity($loginForm->getValue('username'));
            $adapter->setCredential($loginForm->getValue('password'));
 
            $result = Zend_Auth::getInstance()->authenticate($adapter);
 
            if ($result->isValid()) {
            		
            	  $sHandle = Zend_Auth::getInstance()->getIdentity();
	      				$mUser = new Application_Model_Users();
	      				$rUser = $mUser->fetchRow( $mUser->select()->where('handle LIKE "' . $sHandle . '"') );
								
								Zend_Registry::get('sag')->curUser = $rUser->toArray();				
	      				
                $this->_helper->FlashMessenger('Erfolgreich angemeldet.');
                															 
				        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
					          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
					          																 'controller'=>$this->getRequest()->controller, 
					          																 'action' => $this->getRequest()->action)
					          																 );                															 
            }else{
                $this->_helper->FlashMessenger('Login ging schief.');

                Zend_Registry::get('log')->log('Login failed', Zend_Log::INFO);         
            }
            
            $this->_redirect('/');
                
            return; 
        } else {
            $loginForm->setAction('/auth/login');
            $this->view->placeholder('loginForm')->append($loginForm);
        }                
    }
    
    public function logoutAction()
    {
    	  Zend_Registry::get('log')->log('Logout done',
                															 Zend_Log::INFO,
                															 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id']) );
                															 
        Zend_Auth::getInstance()->clearIdentity();
				Zend_Session::destroy();
        
        $this->_redirect('/');
                
        return; 
    }
}

