<?php

class ProfileController extends Zend_Controller_Action
{

		private $profile_forms;
	
    public function init()
    {
    	  $this->arrProfForms = array( 'userdataForm','educationForm',
    	  														 'householdForm','consumeForm',
    	  														 'mobilityForm','lifestyleForm');        
    }    
    
    public function indexAction()
    {    	
    		// fetch user credential informations from DB
				$mUserData = new Application_Model_Users();
				$rUserData = $mUserData->fetchRow( $mUserData->select()->where('user_id = ' . $this->getUserId()) );
    	
        // fetch profile informations from DB
	      $mUserProfile = new Application_Model_UserProfile();	      
	      $rUserProfile = $mUserProfile->fetchRow( $mUserProfile->select()->where('users_user_id = ' . $this->getUserId()) );
	      
	      if(is_object($rUserProfile))
	      {
	      	$userData = $rUserProfile->toArray();
	      	$userData['username'] = $rUserData->handle;
	      }	      
	      
	      // setup placeholder forms for tabs
				foreach($this->arrProfForms as $sFormName)
				{
					  $this->setTabForm($sFormName, (array)$userData );	
				}								
				
				$profileForm = $this->getRequest()->getParam('form');
				
				if($profileForm != "") $this->view->selectedForm = $profileForm;
				
        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );				
    }
    
		/**
     * Generic method to generate form placeholder which
     * can be populated in coresponding view template.
     * 
     * @param $formcall String Name of form description stored in forms folder
     * @param $formValues Array Form values  
     * @return void 
     */
    private function setTabForm($formcall, $formValues)
    {
	      $userProfileForm = new Application_Form_UserProfile($_POST);
	      $userProfileForm->$formcall();
	      $userProfileForm->populate($formValues);
	      $userProfileForm->setAction('/profile/saveuserprofile/form/'.$formcall);
	      $this->view->placeholder($formcall)->append($userProfileForm);
    }
    
    public function saveuserprofileAction()
    {        
        $profileForm = $this->getRequest()->getParam('form');
    	
        if(in_array($profileForm, $this->arrProfForms))
        {
		    		// get corresponding form
						$userProfileForm = new Application_Form_UserProfile($_POST);
			      $userProfileForm->$profileForm();
			      
			      if ($userProfileForm->isValid($_POST)) {	      							 			      	
			      	 // update values in DB
							 $mUserProfile = new Application_Model_UserProfile();							 
							 $formValues = $userProfileForm->getValues();
							 
							 // update user table
							 $mUser = new Application_Model_Users();
							 $where = $mUser->getAdapter()->quoteInto('user_id = ?', $this->getUserId());
							 $credentials = array();
							 // setup values array
							 if(!empty($formValues['username']) && 
							 		$formValues['username'] != Zend_Registry::get('sag')->curUser['handle'])
							 {
							 		$credentials['handle'] =  $formValues['username'];	
							 		$mUser->update($credentials, $where);
							 		$adapter = new Zend_Auth_Adapter_DbTable($this->_getParam('db'), 'users', 'handle', 'password', '?');							 		
            			$adapter->setIdentity($formValues['username']);            			
            			$adapter->setCredential(Zend_Registry::get('sag')->curUser['password']);
            			$result = Zend_Auth::getInstance()->authenticate($adapter);							 		
            			Zend_Registry::get('sag')->curUser['handle'] = $formValues['username'];
							 }							 
							 
							 if($formValues['password'] != ""&& 
							 		md5($formValues['password']) != Zend_Registry::get('sag')->curUser['password'])
							 {
							 	 $credentials['password'] = md5($formValues['password']);
							 	 Zend_Registry::get('sag')->curUser['password'] = $credentials['password']; 
							 	 $mUser->update($credentials, $where);
							 }						
							 
							 // kill username and password field, just in case
							 unset($formValues['username']);
							 unset($formValues['password']);
							 $where = $mUserProfile->getAdapter()->quoteInto('users_user_id = ?', $this->getUserId());
							 $mUserProfile->update($formValues, $where);
			      }
		        $this->_redirect('profile/index/form/' . $profileForm);
		        
        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );		        
        } else {        	
        	echo "ERROR: Form not valid.";
        	exit;
        }        
    }
    
    public function saveinterestsAction()
    {
    	  // get parent interest ID
				$parentId = $this->getRequest()->getParam('parentid');
    	  // get dynamic form
				$mInterests = new Application_Model_Interests();
				$form = $this->getCheckboxesForm($parentId, 
	          										         $mInterests->fetchSubInterests($parentId));
	          										         
    		if ($form->isValid($_POST)) {	      	 
	      	 // update values in DB
					 $mInterests->updateUserInterests($this->getUserId(), $form->getValues());
					 
					 $this->_redirect('profile/interests/parentid/' . $parentId);
	      }	          										         
	      
        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );	      
    }

    public function interestsAction()
    {
        // get all stored interests
	      $mInterests = new Application_Model_Interests();
	      $interests['parents'] = $mInterests->fetchParentInterests(); 

	      // get all user stored interests
				$checkedBoxes = array();
				$usrInterests = $mInterests->fetchUserInterests($this->getUserId());
				foreach($usrInterests as $usrInterest)
				{
					 $checkedBoxes[$usrInterest['id']] = $usrInterest['id']; 
				}

        // get all sub interests
	      foreach($interests['parents'] as $interest)
	      {
	          $interests['forms'][$interest['id']] = $this->getCheckboxesForm($interest['id'], 
	          																																						$mInterests->fetchSubInterests($interest['id']),
	          																																						$checkedBoxes );
	      }	      
	      
	      $this->view->interests = $interests;
	      
	      $parentId = $this->getRequest()->getParam('parentid');
				
				if($parentId != "") $this->view->parentId = $parentId;
				
        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );				
    }
    
    private function getCheckboxesForm($parentId, $interests, $usrInterests = array())
		{
		    $cbs = array();
		    $form = new Zend_Dojo_Form();
		    $form->setName('interests' . $parentId);
		    $form->setMethod('post');
		    $form->setAction('profile/saveinterests/parentid/' . $parentId);     		    
		    
				foreach($interests as $interest)
				{					
					  $cb = new Zend_Dojo_Form_Element_CheckBox($interest['id'], 
					  																		array('checkedValue' => $interest['id'], 
					  																					'uncheckedValue' => '0',
					  																					'label' => $interest['label']));
        		$form->addElement($cb);					  
				}
				
				$submit = new Zend_Dojo_Form_Element_SubmitButton('submit' . $parentId, array('label'=>'Speichern', 'ignore'=>true));
				$submit->addDecorator('HtmlTag', array('tag' => 'div', 'class' => 'buttonbar'));
				
				$form->addElement($submit);
				$form->populate($usrInterests);
		    
				return $form;    	
    }
    
		private function getUserId()
    {
				return Zend_Registry::get('sag')->curUser['user_id'];
    }
}