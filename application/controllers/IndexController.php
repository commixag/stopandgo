<?php

class IndexController extends Zend_Controller_Action
{

    protected $_flashMessenger = null;
    
    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->_flashMessenger->setNamespace('flashMessage');
    }

    public function indexAction()
    {
        if(!Zend_Auth::getInstance()->hasIdentity()) {
	          $loginForm = new Application_Form_Login($_POST);
	          $loginForm->setAction('/auth/login');
	          $this->view->placeholder('loginForm')->append($loginForm);
        } else {
            $searchForm = new Application_Form_Search();
            $this->view->placeholder('searchForm')->append($searchForm);
            
            $mMaterial = new Application_Model_Material();
            $this->view->tabs = $mMaterial->getUserMaterial();
        }
        
        Zend_Registry::get('log')->log('URI: '.$_SERVER['REQUEST_URI'], Zend_Log::INFO,
	          													 array('user_id'=>Zend_Registry::get('sag')->curUser['user_id'],
	          																 'controller'=>$this->getRequest()->controller, 
	          																 'action' => $this->getRequest()->action)
	          																 );
    }
}

