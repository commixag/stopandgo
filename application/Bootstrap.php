<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	  protected function _initLog()
	  {
    		$this->bootstrap('Db');		    	

    		$columnMapping = array(	'timestamp' => 'timestamp',
    														'priority' => 'priority', 
    														'msg' => 'message',
    														'priority_name' => 'priorityName',
    														'users_user_id' => 'user_id',
    														'controller' => 'controller',
    														'action' => 'action' );
    														
			  $writer = new Zend_Log_Writer_Db(Zend_Db_Table::getDefaultAdapter(), "logs", $columnMapping);
			  
			  $log = new Zend_Log();
			  $log->addWriter($writer);
			
			  Zend_Registry::set("log", $log);
			      
			  return $log;
	  }
	
		protected function _initSession()
		{
				Zend_Session::start();
				$sag = new Zend_Session_Namespace('sag');
				
				$reg = Zend_Registry::getInstance();
				$reg->set('sag', $sag);
		}
	
    protected function _initDoctype()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');

        // DOJO Integration
        Zend_Dojo::enableView($view);
        Zend_Dojo_View_Helper_Dojo::setUseDeclarative();
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        $viewRenderer->setView($view);
    }

}

