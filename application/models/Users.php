<?php
/**
*
* Class name: Application_Model_Users
*
* @description DB connection Model
* @author Dresch <dresch@unknown.de>
* @package Stop And Go Prototype
* 
*/

class Application_Model_Users extends Zend_Db_Table
{
	protected $_name = 'users';
	protected $_primary = 'user_id';
	
	protected $_dependentTables = array('UsersHasInterest');
}

?>