<?php

class Application_Model_Groupsale
{

    public function getTabs()
    {
        $data = array();
			  $data[] = array('title'=>'61% Fitness','pic'=>'images/groupbox1.jpg','descr'=>'61% auf Fitness-Abo für 6 Monate.<br /><br />
			  Braucht noch 3 Käufer<br />Läuft weniger als 1 Tag.',
			  	'url'=>array('controller'=>'page','action'=>'fitness'));
			  $data[] = array('title'=>'60% Tanzkurs','pic'=>'images/groupbox2.jpg','descr'=>'Jetzt 60% sparen auf Tanzschule.<br /><br />
			  Braucht noch 2 Käufer.<br />Läuft noch 2 Tage.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $data[] = array('title'=>'50% Wild West','pic'=>'images/groupbox3.jpg','descr'=>'Ride and enjoy for half price.<br /><br />
			  Braucht noch 1 Käufer.<br />Läuft noch 2 Tage.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $data[] = array('title'=>'52% Helikopter','pic'=>'images/groupbox4.jpg','descr'=>'Die Vogelperspektive mit 52% Rabatt.<br /><br />
			  Braucht noch 4 Käufer.<br />Läuft noch 3 Tage.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $data[] = array('title'=>'54% Scooter','pic'=>'images/groupbox5.jpg','descr'=>'Zwei tolle Räder. 54% reduziert.<br /><br />
			  Braucht noch 12 Käufer.<br />Läuft noch 4 Tage.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $data[] = array('title'=>'54% Vino Toscana','pic'=>'images/groupbox6.jpg','descr'=>'54 Prozent auf feinem Toskaner.<br /><br />
			  Braucht noch 1 Käufer.<br />Läuft noch 5 Tage.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $data[] = array('title'=>'54% Black Socks','pic'=>'images/groupbox7.jpg','descr'=>'Socken an die Füsse mit 54%.<br /><br />
			  Braucht noch 7 Käufer.<br />Läuft noch 6 Tage.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $data[] = array('title'=>'50% Rindsfilet','pic'=>'images/groupbox8.jpg','descr'=>'Das feine Stück zum halben Preis.<br /><br />
			  Braucht noch 3 Käufer.<br />Läuft noch 7 Tage.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			
			  $sport = array();
			  $sport[] = array('title'=>'migros - KW14','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $sport[] = array('title'=>'migros - KW15','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $sport[] = array('title'=>'migros - KW16','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $sport[] = array('title'=>'migros - KW17','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $sport[] = array('title'=>'migros - KW18','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $sport[] = array('title'=>'migros - KW19','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $sport[] = array('title'=>'migros - KW20','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  $sport[] = array('title'=>'migros - KW21','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			  	'url'=>array('controller'=>'page','action'=>'fondue'));
			  
			  $tabs = array();
			  $tabs[] = array('label'=>'Letzte Chance',  'data'=>$data);
			  $tabs[] = array('label'=>'Neuste',  'data'=>$data);
			  $tabs[] = array('label'=>'Sport',    'data'=>$sport);
			  $tabs[] = array('label'=>'Freizeit', 'data'=>$data);
			  $tabs[] = array('label'=>'Computer', 'data'=>$data);
			  
			  return $tabs;
    }
    
    public function getGroupsaleDetails($id)
    {
        return array(
                        'title'=>'Bettmeralp',
                        'descr'=>'Der Knaller: ein Wochenende im Schnee ohne Risiken.',
                        'pic'=>'images/Box_6.jpg'
        
                    );
    }
}

