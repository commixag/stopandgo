<?php

class Application_Model_Material
{

    public function getTabs()
    {
        $data = array();
			  $data[] = array('title'=>'Fust Post','pic'=>'images/Box_4.jpg','descr'=>'Mit Sonderangeboten zu Tiefstpreisen.<br><br>',
			                  'url'=>array('controller'=>'page','action'=>'fust'));
			  $data[] = array('title'=>'Denner Woche','pic'=>'images/material_dennerbox.jpg','descr'=>'Ausgabe 5/2011. <br />Gültig ab 2. Februar.<br><br>',
                        'url'=>array('controller'=>'page','action'=>'denner'));
			  $data[] = array('title'=>'Otto\'s','pic'=>'images/Box_5.jpg','descr'=>'Der neuste Katalog jetzt erhältlich.<br><br>',
			                  'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $data[] = array('title'=>'Coopzeitung','pic'=>'images/Box_1.jpg','descr'=>'Coopzeitung Nr. 5 vom 1. Februar 2011.<br><br>',
			                  'url'=>array('controller'=>'page','action'=>'coopzeitung'));
			  
			  $data[] = array('title'=>'Media Markt','pic'=>'images/material_mediamarkt.jpg','descr'=>'Ich bin doch nicht blöd.<br />Die neusten Angebote.<br /><br />',
                        'url'=>array('controller'=>'page','action'=>'empty'));
			  $data[] = array('title'=>'Städte und Punkte','pic'=>'images/material_box3.jpg','descr'=>'Ein CUMULUS-Punkt pro Franken.<br /><br />',
			                  'url'=>array('controller'=>'page','action'=>'empty'));
			  $data[] = array('title'=>'Migros-Magazin','pic'=>'images/material_migrosmagazin.jpg','descr'=>'Die neue Ausgabe jetzt ansehen.<br /><br />',
			                  'url'=>array('controller'=>'page','action'=>'empty'));
			  $data[] = array('title'=>'Bauhaus','pic'=>'images/Box_7.jpg','descr'=>'Die besten Produkte für den anspruchsvollen Heimwerker.<br />',
                       			 'url'=>array('controller'=>'page','action'=>'empty'));
			  
			  $data[] = array('title'=>'Calida','pic'=>'images/Box_8.jpg','descr'=>'Die neue Dessous-Kollektion für Anspruchsvolle.',
                       			 'url'=>array('controller'=>'page','action'=>'empty'));
			  $data[] = array('title'=>'UVEX','pic'=>'images/material_sportbox1.jpg','descr'=>'Alle Skibrillen 30% günstiger, solange Vorrat.',
                        		 'url'=>array('controller'=>'page','action'=>'empty'));
			  $data[] = array('title'=>'PKZ','pic'=>'images/material_pkz.jpg','descr'=>'Der neuste Katalog für Männer-Mode.<br /><br />',
			                   'url'=>array('controller'=>'page','action'=>'empty'));
			  $data[] = array('title'=>'Nidecker','pic'=>'images/material_sportbox3.jpg','descr'=>'Alle 2010-Modelle zu stark reduzierten Preisen.',
			                   'url'=>array('controller'=>'page','action'=>'empty'));
			  
			  $data[] = array('title'=>'Lidl','pic'=>'images/material_lidl.jpg','descr'=>'Die Angebote für die nächste Woche.<br /><br />',
			                   'url'=>array('controller'=>'page','action'=>'empty'));
			  $data[] = array('title'=>'SportXX','pic'=>'images/material_sportbox5.jpg','descr'=>'Der Ausverkauf: Top-Modelle zu Top-Preisen.',
			                   'url'=>array('controller'=>'page','action'=>'empty'));
			  $data[] = array('title'=>'Aldi Woche','pic'=>'images/material_aldiwoche.jpg','descr'=>'Alle Tiefstpreisangebote auf einen Blick.<br /><br />',
			                   'url'=>array('controller'=>'page','action'=>'empty'));
			  $data[] = array('title'=>'Ackermann','pic'=>'images/material_ackermann.jpg','descr'=>'Der neue Katalog in Deutsch.<br />.',
                        'url'=>array('controller'=>'page','action'=>'empty'));
			  
			  
			
			  $sport = array();
			  $sport[] = array('title'=>'UVEX','pic'=>'images/material_sportbox1.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                         'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $sport[] = array('title'=>'Schlittschuhe','pic'=>'images/material_sportbox2.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                   'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $sport[] = array('title'=>'Snowboard','pic'=>'images/material_sportbox3.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                   'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $sport[] = array('title'=>'Fahrrad','pic'=>'images/material_sportbox4.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                   'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $sport[] = array('title'=>'SportXX','pic'=>'images/material_sportbox5.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                   'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $sport[] = array('title'=>'Käse und Brot','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                   'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $sport[] = array('title'=>'migros - KW20','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                   'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $sport[] = array('title'=>'migros - KW21','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                   'url'=>array('controller'=>'page','action'=>'publidetail'));
			  
			  $freizeit = array();
			  $freizeit[] = array('title'=>'migros - KW14','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                      'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $freizeit[] = array('title'=>'migros - KW15','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                      'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $freizeit[] = array('title'=>'migros - KW16','pic'=>'images/material_box3.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                      'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $freizeit[] = array('title'=>'migros - KW17','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                      'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $freizeit[] = array('title'=>'migros - KW18','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                      'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $freizeit[] = array('title'=>'migros - KW19','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                      'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $freizeit[] = array('title'=>'migros - KW20','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                      'url'=>array('controller'=>'page','action'=>'publidetail'));
			  $freizeit[] = array('title'=>'migros - KW21','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
			                      'url'=>array('controller'=>'page','action'=>'publidetail'));
			  
			  $tabs = array();
			  $tabs[] = array('label'=>'Neu',  'data'=>$data);
			  $tabs[] = array('label'=>'Sport',    'data'=>$sport);
			  $tabs[] = array('label'=>'Freizeit', 'data'=>$freizeit);
			  $tabs[] = array('label'=>'Haustiere', 'data'=>$sport);
			  $tabs[] = array('label'=>'Küche', 'data'=>$freizeit);
			  $tabs[] = array('label'=>'Reisen', 'data'=>$sport);
			  $tabs[] = array('label'=>'Elektronik', 'data'=>$freizeit);
			  $tabs[] = array('label'=>'Events', 'data'=>$sport);
			  $tabs[] = array('label'=>'Möbel', 'data'=>$freizeit);
			  $tabs[] = array('label'=>'Finanzen', 'data'=>$sport);
			  $tabs[] = array('label'=>'Gesundheit', 'data'=>$freizeit);
			  $tabs[] = array('label'=>'Energie', 'data'=>$sport);
			  
			  return $tabs;
    }
    
    public function getUserMaterial($usrId = 0)
    {
        $data = array();
        $data[] = array('title'=>'Coopzeitung','pic'=>'images/Box_1.jpg','descr'=>'Coopzeitung Nr. 5 vom 1. Februar 2011.<br /><br />',
                        'url'=>array('controller'=>'page','action'=>'coopzeitung'));
        $data[] = array('title'=>'Otto\'s','pic'=>'images/Box_5.jpg','descr'=>'Der neuste Katalog jetzt erhältlich.<br /><br />',
                        'url'=>array('controller'=>'page','action'=>'publidetail'));
        $data[] = array('title'=>'Aldi Woche','pic'=>'images/material_aldiwoche.jpg','descr'=>'Alle Tiefstpreisangebote auf einen Blick.<br /><br />',
                        'url'=>array('controller'=>'page','action'=>'fondue'));
        $data[] = array('title'=>'Städte und Punkte','pic'=>'images/material_box3.jpg','descr'=>'Ein CUMULUS-Punkt pro Franken.<br /><br />',
                        'url'=>array('controller'=>'page','action'=>'fondue'));
        $data[] = array('title'=>'Migros-Magazin','pic'=>'images/material_migrosmagazin.jpg','descr'=>'Die neue Ausgabe jetzt ansehen.<br /><br />',
                        'url'=>array('controller'=>'page','action'=>'fondue'));
        $data[] = array('title'=>'Bettmeralp','pic'=>'images/Box_6.jpg','descr'=>'Die besten Skiangebote jetzt buchen.<br /><br />',
                        'url'=>array('controller'=>'page','action'=>'fondue'));
        $data[] = array('title'=>'Bauhaus','pic'=>'images/Box_7.jpg','descr'=>'Der neue Prospekt mit unschlagbaren Angeboten.<br />',
                        'url'=>array('controller'=>'page','action'=>'fondue'));
        $data[] = array('title'=>'Calida','pic'=>'images/Box_8.jpg','descr'=>'Die neue Dessous-Kollektion.<br /><br />',
                        'url'=>array('controller'=>'page','action'=>'fondue'));
      
        $sport = array();
        $sport[] = array('title'=>'Thömus','pic'=>'images/material_sportbox7.jpg','descr'=>'Der aktuelle Winterkatalog 2010/2011.',
                         'url'=>array('controller'=>'page','action'=>'thoemus'));
        $sport[] = array('title'=>'SPORTXXX','pic'=>'images/material_sportbox5.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                         'url'=>array('controller'=>'page','action'=>'publidetail'));
        $sport[] = array('title'=>'Vaucher','pic'=>'images/material_sportbox6.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                         'url'=>array('controller'=>'page','action'=>'publidetail'));
        $sport[] = array('title'=>'adidas','pic'=>'images/material_sportbox8.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                         'url'=>array('controller'=>'page','action'=>'publidetail'));
        $sport[] = array('title'=>'Uvex','pic'=>'images/material_sportbox1.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                         'url'=>array('controller'=>'page','action'=>'publidetail'));
        $sport[] = array('title'=>'migros - KW15','pic'=>'images/material_sportbox2.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                         'url'=>array('controller'=>'page','action'=>'publidetail'));
        $sport[] = array('title'=>'migros - KW16','pic'=>'images/material_sportbox3.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                         'url'=>array('controller'=>'page','action'=>'publidetail'));
        $sport[] = array('title'=>'migros - KW17','pic'=>'images/material_sportbox4.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                         'url'=>array('controller'=>'page','action'=>'publidetail'));
        
        
               
        $garden = array();
        $garden[] = array('title'=>'Landi','pic'=>'images/material_gardenbox1.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'garden1'));
        $garden[] = array('title'=>'Landi','pic'=>'images/material_gardenbox2.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'garden1'));
        $garden[] = array('title'=>'Landi','pic'=>'images/material_gardenbox3.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'garden1'));
        $garden[] = array('title'=>'Landi','pic'=>'images/material_gardenbox4.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'garden1'));
        $garden[] = array('title'=>'migros - KW18','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'garden1'));
        $garden[] = array('title'=>'migros - KW19','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'garden1'));
        $garden[] = array('title'=>'migros - KW20','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'garden1'));
        $garden[] = array('title'=>'migros - KW21','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'garden1'));
        
        $newsletter = array();
        $newsletter[] = array('title'=>'Gräb','pic'=>'images/material_newsletter1.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'empty'));
        $newsletter[] = array('title'=>'Dior','pic'=>'images/material_newsletter2.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'empty'));
        $newsletter[] = array('title'=>'Tommy Hilfiger','pic'=>'images/material_newsletter3.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'empty'));
        $newsletter[] = array('title'=>'Haslital','pic'=>'images/material_newsletter4.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'empty'));
        $newsletter[] = array('title'=>'CillitBang','pic'=>'images/material_newsletter5.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'empty'));
        $newsletter[] = array('title'=>'Nespresso','pic'=>'images/material_newsletter6.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'empty'));
        $newsletter[] = array('title'=>'Jelmoli','pic'=>'images/material_newsletter7.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'empty'));
        $newsletter[] = array('title'=>'Orell Füssli','pic'=>'images/material_newsletter8.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
                            'url'=>array('controller'=>'page','action'=>'empty'));
        
        $tabs = array();
        $tabs[] = array('label'=>'Neu',  'data'=>$data);
        $tabs[] = array('label'=>'Sport',    'data'=>$sport);
        $tabs[] = array('label'=>'Garten', 'data'=>$garden);
        $tabs[] = array('label'=>'Newsletter', 'data'=>$newsletter);
        $tabs[] = array('label'=>'Empfohlen', 'data'=>$garden);
        $tabs[] = array('label'=>'Gruppen', 'data'=>$sport);
        $tabs[] = array('label'=>'Specials', 'data'=>$garden);
        
        return $tabs;
    }
        
    public function getProviderMaterial($prvId = 0)
    {   
    		// initialize for later check if data is given 	
    		$tabs = false;
    		
    		// OTTO's
    	  if($prvId == 58)
    	  {    	  	
		        $data = array();
					  $data[] = array('title'=>'Otto\'s Prospekt','pic'=>'images/Box_5.jpg','descr'=>'Die aktuellen Angebote für die laufende Woche.<br /><br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $data[] = array('title'=>'Otto\'s Home','pic'=>'images/material_ottoshome.jpg','descr'=>'Schlüsselfertig aus einer Hand. <br /><br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $data[] = array('title'=>'Otto\'s CARS','pic'=>'images/material_ottoscars.jpg','descr'=>'Das Angebot der Woche.<br /><br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $data[] = array('title'=>'Otto\'s Möbel','pic'=>'images/material_ottosmoebel.jpg','descr'=>'Der ultimative Möbelkatalog.<br /><br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));					  
					
					  $news = array();
					  $news[] = array('title'=>'Otto\'s Electronics','pic'=>'images/material_ottosnews.jpg','descr'=>'1 x p/Woche<br />7 Pts<br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $news[] = array('title'=>'Otto\'s Automiete','pic'=>'images/material_ottosnews.jpg','descr'=>'1 x p/Monat<br />7 Pts<br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $news[] = array('title'=>'Otto\'s Reisen','pic'=>'images/material_ottosnews.jpg','descr'=>'1 x p/Monat<br />7 Pts<br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $news[] = array('title'=>'Otto\'s Garten','pic'=>'images/material_ottosnews.jpg','descr'=>'1 x p/Woche<br />7 Pts<br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  
					  $tabs = array();
					  $tabs[] = array('label'=>'Prospekte',  'data'=>$data);
					  $tabs[] = array('label'=>'Newsletter', 'data'=>$news);
    	  }
    	  // Fust
    	  else if($prvId == 42)
    	  {
    	  		$data = array();
					  $data[] = array('title'=>'Fust Post','pic'=>'images/Box_4.jpg','descr'=>'Die aktuellen Angebote für die laufende Woche.<br /><br />',
					  	'url'=>array('controller'=>'page','action'=>'fust'));
					  $data[] = array('title'=>'Fust\'s Home','pic'=>'images/Box_4.jpg','descr'=>'Schlüsselfertig aus einer Hand. <br /><br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $data[] = array('title'=>'Fust\'s CARS','pic'=>'images/Box_4.jpg','descr'=>'Das Angebot der Woche.<br /><br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $data[] = array('title'=>'Fust\'s Möbel','pic'=>'images/Box_4.jpg','descr'=>'Der ultimative Möbelkatalog.<br /><br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));					  
					
					  $news = array();
					  $news[] = array('title'=>'Fust\'s Electronics','pic'=>'images/material_ottosnews.jpg','descr'=>'1 x p/Woche<br />7 Pts<br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $news[] = array('title'=>'Fust\'s Automiete','pic'=>'images/material_ottosnews.jpg','descr'=>'1 x p/Monat<br />7 Pts<br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $news[] = array('title'=>'Fust\'s Reisen','pic'=>'images/material_ottosnews.jpg','descr'=>'1 x p/Monat<br />7 Pts<br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $news[] = array('title'=>'Fust\'s Garten','pic'=>'images/material_ottosnews.jpg','descr'=>'1 x p/Woche<br />7 Pts<br />',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  
					  $tabs = array();
					  $tabs[] = array('label'=>'Prospekte',  'data'=>$data);
					  $tabs[] = array('label'=>'Newsletter', 'data'=>$news);
    	  }
			  
			  return $tabs;
    }
}

