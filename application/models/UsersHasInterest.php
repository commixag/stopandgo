<?php
class Application_Model_UsersHasInterest extends Zend_Db_Table_Abstract
{
    protected $_name = 'users_has_interest';
 
    protected $_referenceMap    = array(
        'Users' => array(
            'columns'           => array('users_user_id'),
            'refTableClass'     => 'Application_Model_Users',
            'refColumns'        => array('user_id')
        ),
        'Interests' => array(
            'columns'           => array('interest_id'),
            'refTableClass'     => 'Application_Model_Interests',
            'refColumns'        => array('id')
        )
    );
 
}
?>