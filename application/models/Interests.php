<?php
/**
*
* Class name: Application_Model_Interests
*
* @description DB connection Model
* @author Dresch <dresch@unknown.de>
* @package Stop And Go Prototype
* 
*/


class Application_Model_Interests extends Zend_Db_Table
{
    protected $_name = 'interests';
	  protected $_primary = 'id';
	  
	  protected $_dependentTables = array('UsersHasInterest');
	  
    public function getInterestsList()
    {
        $results = $this->fetchAll();
        
        return $results;
    }
	  
	  /**
	   * Return interest item id of given interest label.
	   * 
	   * @return integer 
	   */
    public function getIdFromLabel($label)
    {
        $row = $this->fetchRow($this->select('id')
                                    ->where('label = ?', $label));
        if($row === null)
        {
            return null; 
        } else {
            $data = $row->toArray();
            return $data['id'];                                   
        }                                          
    }
	  
	  /**
	   * Get array with all parent interests.
	   * 
	   * @return array
	   */
	  public function fetchParentInterests() 
	  {
	      $rInterests = $this->fetchAll($this->select()->where('parent_id IS NULL'));
	      return $rInterests->toArray();  	    
	  }	  	  
	  
	  /**
	   * Get array with all interests of given parent_id.
	   * 
	   * @return array
	   */
	  public function fetchSubInterests($parent_id) 
	  {
	      $rInterests = $this->fetchAll($this->select()->where('parent_id = ' . $parent_id));
	      return $rInterests->toArray();  	    
	  }
	  
		/**
	   * Get array with all interests of given parent_id.
	   * 
	   * @return array
	   */
	  public function fetchUserInterests($user_id) 
	  {
	  		$mUsers = new Application_Model_Users();
				$rUser = $mUsers->find($user_id);
				$user = $rUser->current();
 
				$userInterests = $user->findManyToManyRowset('Application_Model_Interests', 'Application_Model_UsersHasInterest');
	      
	      return $userInterests->toArray();  	    
	  }
	  
	  public function updateUserInterests($user_id, $interest_ids)
	  {
	  	  $mUsersHasInterest = new Application_Model_UsersHasInterest();
	  	  foreach($interest_ids as $id=>$value)
	  	  {
	  	  	  $mUsersHasInterest->delete('users_user_id = '. $user_id . ' AND interest_id = ' . $id);
	  	  	  if($value != '0'){
	  	  	  	$mUsersHasInterest->insert(array('users_user_id'=> $user_id, 'interest_id' => $id));
	  	  	  }
	  	  }
	  }
}

