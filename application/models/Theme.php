<?php
/**
*
* Class name: Application_Model_Theme
*
* @description DB connection Model
* @author Dresch <dresch@unknown.de>
* @package Stop And Go Prototype
* 
*/


class Application_Model_Theme extends Zend_Db_Table
{
    protected $_name = 'theme';
	  protected $_primary = 'id';
	  
    public function getList()
    {
        $results = $this->fetchAll($this->select()->order('label'));
        
        return $results;
    }
    
    public function getSublist($tId)
    {    	
    	  // initialize for later check if data is given 	
    		$tabs = false;
    		
    		// Schlafzimmer
    	  if($tId == 11)
    	  {    	  
			   	  $data = array();
					  $data[] = array('title'=>'Lille','pic'=>'images/material_schlafbox1.jpg','descr'=>'Mit schwarzem Leder, für Anspruchsvolle.<br>',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Nantes','pic'=>'images/material_schlafbox2.jpg','descr'=>'Eleganz im Schlafzimmer, edel.',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Nizza','pic'=>'images/material_schlafbox3.jpg','descr'=>'Programm für die Lust an Farben.',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Milano','pic'=>'images/material_schlafbox4.jpg','descr'=>'Für Kenner der italiensichen Vorzüge.',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Lilac','pic'=>'images/material_schlafbox5.jpg','descr'=>'Ausschweifendes Programm in Grün.',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Mioletto','pic'=>'images/material_schlafbox6.jpg','descr'=>'Für Liebhaber von Stil und Ordnung.',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'CASAnotte','pic'=>'images/material_schlafbox7.jpg','descr'=>'Programm für Romantiker.<br>',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Multi-Forma','pic'=>'images/material_schlafbox8.jpg','descr'=>'Praktisch und multifunktional.',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					
					  $sport = array();
					  $sport[] = array('title'=>'Otto\'s Electronics','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $sport[] = array('title'=>'Otto\'s Automiete','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $sport[] = array('title'=>'Otto\'s Reisen','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $sport[] = array('title'=>'Otto\'s Garten','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  
					  $tabs = array();
					  $tabs[] = array('label'=>'Schlafzimmerprogramme',  'data'=>$data);
					  $tabs[] = array('label'=>'Betten',    'data'=>$sport);
					  $tabs[] = array('label'=>'Kissen und Decken',  'data'=>$data);
					  $tabs[] = array('label'=>'Möbel',    'data'=>$sport);
					  $tabs[] = array('label'=>'Dekorationen',  'data'=>$data);
					  $tabs[] = array('label'=>'Kinder',    'data'=>$sport);
				}
    	  // Bildung
    	  else if($tId == 7)
    	  {
    	  		$data = array();
					  $data[] = array('title'=>'Bildung','pic'=>'images/material_schlafbox1.jpg','descr'=>'Mit schwarzem Leder, für Anspruchsvolle.<br>',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Bildung','pic'=>'images/material_schlafbox2.jpg','descr'=>'Eleganz im Schlafzimmer, edel.',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Bildung','pic'=>'images/material_schlafbox3.jpg','descr'=>'Programm für die Lust an Farben.',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Bildung','pic'=>'images/material_schlafbox4.jpg','descr'=>'Für Kenner der italiensichen Vorzüge.',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Bildung','pic'=>'images/material_schlafbox5.jpg','descr'=>'Ausschweifendes Programm in Grün.',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Bildung','pic'=>'images/material_schlafbox6.jpg','descr'=>'Für Liebhaber von Stil und Ordnung.',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Bildung','pic'=>'images/material_schlafbox7.jpg','descr'=>'Programm für Romantiker.<br>',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					  $data[] = array('title'=>'Bildung','pic'=>'images/material_schlafbox8.jpg','descr'=>'Praktisch und multifunktional.',
					  	'url'=>array('controller'=>'page','action'=>'lille'));
					
					  $sport = array();
					  $sport[] = array('title'=>'Otto\'s Electronics','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $sport[] = array('title'=>'Otto\'s Automiete','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $sport[] = array('title'=>'Otto\'s Reisen','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  $sport[] = array('title'=>'Otto\'s Garten','pic'=>'images/dummy02.jpg','descr'=>'Dies ist eine Beschreibung die das Material beschreibt.',
					  	'url'=>array('controller'=>'page','action'=>'publidetail'));
					  
					  $tabs = array();
					  $tabs[] = array('label'=>'Schlafzimmerprogramme',  'data'=>$data);
					  $tabs[] = array('label'=>'Betten',    'data'=>$sport);
					  $tabs[] = array('label'=>'Kissen und Decken',  'data'=>$data);
					  $tabs[] = array('label'=>'Möbel',    'data'=>$sport);
					  $tabs[] = array('label'=>'Dekorationen',  'data'=>$data);
					  $tabs[] = array('label'=>'Kinder',    'data'=>$sport);
    	  }
		    
		    return $tabs;
    }
}

