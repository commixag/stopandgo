<?php
/**
*
* Class name: Application_Model_Provider
*
* @description DB connection Model
* @author Dresch <dresch@unknown.de>
* @package Stop And Go Prototype
* 
*/


class Application_Model_Provider extends Zend_Db_Table
{
    protected $_name = 'provider';
	  protected $_primary = 'id';
	  
    public function getList()
    {
        $results = $this->fetchAll($this->select()->order('label'));
        
        return $results;
    }
}

