<?php

class Application_Form_Search extends Zend_Dojo_Form
{

    public function init()
    {
        $this->setMethod('post');
        $this->setAction('page/searchresults');
        
        $this->addElement( 'ComboBox', 'area_id', array(
                            'label'        => 'Wo suchen?',
                            'autocomplete' => false,
                            'multiOptions' => array( 'choose' => 'Alle Bereiche',
                                                     '5' => 'Kataloge und Prospekte',
        											 '6' => 'Newsletters',
                                                     '7' => 'Anbieter',
        											 '8' => 'Themen',
                                                     '9' => 'Gruppeneinkäufe'),
                            
                            )
                        );
        
        $this->addElement( 'TextBox', 'label', array(
                            'label'          => 'Stichwort?',
                            'required'       => false)
        				);
                                                        
        $this->addElement( 'SubmitButton', 'interest-submit', array(
                           'required'   => false,
                           'ignore'     => true,
                           'label'      => 'Finden')
                        );                                   
                                                        
    }


}