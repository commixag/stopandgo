<?php

class Application_Form_Register extends Zend_Dojo_Form
{

    public function init()
    {
        parent::setName("formRegister");        
        
        $this->setMethod('post');            
 
        $this->addElement( 'ValidationTextBox', 'firstname', array(
                            'label'          => 'Vorname',
                            'required'       => true,
                            'regExp'         => '[\w]+',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );
        
        $this->addElement( 'ValidationTextBox', 'lastname', array(
                            'label'          => 'Nachname',
                            'required'       => true,
                            'regExp'         => '[\w]+',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );
                        
        $this->addElement( 'ValidationTextBox', 'email', array(
                            'label'          => 'E-Mail (temporärer Benutzername)',
                            'required'       => true,
                            'regExp'         => '\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b',
                            'invalidMessage' => 'Ungültige E-Mail Adresse.')
                        );
                        
        $this->addElement( 'PasswordTextBox', 'password', array(
                            'label'          => 'Passwort',
                            'required'       => true,
                            'trim'           => true,
                            'lowercase'      => true,
                            'regExp'         => '^[a-z0-9]{6,}$',
                            'invalidMessage' => 'Ungültiges Passwort; muss mindestens 6 '
                          . 'alphanumerische Zeichen lang sein')
                        );   
                        
        $this->addElement( 'CheckBox', 'legalstatement', array(
                            'label'          => 'Ich habe die Datenschutzbestimmungen gelesen und akzeptiere sie. '
														. 'Die E-Mail-Adresse wird ausschliesslich '
                            . 'von Stop & Go verwendet und nicht weitergegeben.',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        );        
 
        $this->addElement( 'SubmitButton', 'register-submit', array(
                           'required'   => false,
                           'ignore'     => true,
                           'label'      => 'Registrieren')
                        );         
    }


}

