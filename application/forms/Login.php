<?php

class Application_Form_Login extends Zend_Dojo_Form
{

    public function init()
    {
        parent::setName("formLogin");
        
        $this->setMethod('post');            
 
        $this->addElement( 'TextBox', 'username', array(
                            'required'       => true,
                            'label'          => 'Benutzer',
                            'required'       => true)
                        );

        $this->addElement( 'PasswordTextBox', 'password', array(
                            'label'          => 'Passwort',
                            'required'       => true,
                            'trim'           => true,
                            'lowercase'      => true,
                            'regExp'         => '^[a-z0-9]{6,}$',
                            'invalidMessage' => 'Ungültiges Passwort; muß mindestens 6 '
                          . 'alphanumerische Zeichen lang sein')
                        );  
 
        $this->addElement( 'SubmitButton', 'login-submit', array(
                           'required'   => false,
                           'ignore'     => true,
                           'label'      => 'Login!')
                        );                
    }
}

