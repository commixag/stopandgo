<?php

class Application_Form_Interest extends Zend_Dojo_Form
{

    public function init()
    {
        $this->setMethod('post');
        
        $this->addElement( 'ComboBox', 'parent_id', array(
                            'label'        => 'Elternkategorie',
                            'autocomplete' => false
                            )
                        );
        
        $this->addElement( 'TextBox', 'label', array(
                            'label'          => 'Label',
                            'required'       => true));
                            
        $this->addElement( 'TextBox', 'description', array(
                            'label'          => 'Beschreibung',
                            'required'       => false));
                            
        $this->addElement( 'SubmitButton', 'interest-submit', array(
                           'required'   => false,
                           'ignore'     => true,
                           'label'      => 'Speichern')
                        );                                   
                                                        
    }


}

