<?php

class Application_Form_UserProfile extends Zend_Dojo_Form
{

    public function init()
    {    		
        $this->setMethod('post');  
        $this->setDisplayGroupDecorators(array('FormElements', 'Fieldset'));           
    }    

    /** USER DATA LIKE ADDRESS ETC. */
    public function userdataForm()
    {
    	  parent::setName('formUserProfileUserData');
    	
    	  $this->addElement( 'ValidationTextBox', 'username', array(
                            'label'          => 'Benutzername',
                            'required'       => false,
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ@ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );
        
        $this->addElement( 'PasswordTextBox', 'password', array(
                            'label'          => 'Passwort',
                            'required'       => false,
                            'trim'           => true,
                            'lowercase'      => true,
                            'regExp'         => '^[a-z0-9]{6,}$',
                            'invalidMessage' => 'Ungültiges Passwort; muss mindestens 6 '
                          . 'alphanumerische Zeichen lang sein')
                        );                                       
        
        $this->addDisplayGroup(array('username','password','profession'),
                                     'username_password', 
                                     array('legend'=>'Benutzername und Passwort ändern')); 

        $group = $this->getDisplayGroup('username_password');                                     
				$group->setAttrib('class','closed');   
		
				$this->addElement( 'FilteringSelect', 'salutation', array(
                            'label'        => 'Anrede',
                            'autocomplete' => false,
                            'multiOptions' => array( 'noValue' => 'Keine Angabe',
                                                     'Herr' => 'Herr',
                                                     'Frau' => 'Frau'),
                            )
                        );
                        
        $this->addElement( 'FilteringSelect', 'title', array(
                            'label'        => 'Titel',
                            'autocomplete' => false,
                            'multiOptions' => array( 'noValue' => 'Keine Angabe',
                                                     'Dr' => 'Dr.',
                                                     'Prof' => 'Prof.'),
                            )
                        );                        
 
        $this->addElement( 'ValidationTextBox', 'firstname', array(
                            'label'          => 'Vorname',
                            'required'       => true,
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );
        
        $this->addElement( 'ValidationTextBox', 'lastname', array(
                            'label'          => 'Nachname',
                            'required'       => true,
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );    

        $this->addDisplayGroup(array('salutation','title','firstname',
                                     'lastname'),
         							'personal_info',
                                     array('legend'=>'Angaben zu meiner Person'));  

        $group = $this->getDisplayGroup('personal_info');                                     
				$group->setAttrib('class','closed');                             
                                     
        $this->addElement( 'DateTextBox', 'bday', array(
                            'label'          => 'Geburtstag (Format: 25. Juli 1978)',
                            'invalidMessage' => 'Ungültiges Datumsformat.',
                            'formatLength'   => 'long')
                        );     

        $this->addElement( 'FilteringSelect', 'age_class', array(
                            'label'        => 'Altersklasse',
                            'autocomplete' => false,
                            'multiOptions' => array( 'noValue' => 'Keine Angabe',
                                                     'age_class_1' => '15-20 Jahre',
                                                     'age_class_2' => '21-25 Jahre',
											         'age_class_3' => '26-30 Jahre',
											         'age_class_4' => '31-35 Jahre',
											         'age_class_5' => '36-40 Jahre',
											         'age_class_6' => '41-45 Jahre',
											         'age_class_7' => '46-50 Jahre',
											         'age_class_8' => '51-55 Jahre',
											         'age_class_9' => '56-60 Jahre',
											         'age_class_10' => '61-65 Jahre',
         											 'age_class_11' => 'über 65 Jahre'),
                            )
                        ); 
                        
        $this->addElement( 'FilteringSelect', 'starsign', array(
                            'label'        => 'Sternzeichen',
                            'autocomplete' => false,
                            'multiOptions' => array( 'noValue' => 'Keine Angabe',
                                                     'wassermann' => 'Wassermann',
                                                     'fisch' => 'Fisch',
											         'widder' => 'Widder',
											         'stier' => 'Stier',
											         'zwilling' => 'Zwilling',
											         'krebs' => 'Krebs',
											         'loewe' => 'Löwe',
											         'jungfrau' => 'Jungfrau',
											         'waage' => 'Waage',
											         'skorpion' => 'Skorpion',
         											 'schuetze' => 'Schütze',
         											 'steinbock' => 'Steinbock'),
                            )
                        ); 
                                     
        $this->addDisplayGroup(array('bday','age_class','starsign'),
                                     'birthday_age', 
                                     array('legend'=>'Geburts- oder Altersangaben')); 

        $group = $this->getDisplayGroup('birthday_age');                                     
				$group->setAttrib('class','closed');                                                                 

				$this->addElement( 'ValidationTextBox', 'addresstype', array(
                            'label'          => 'Persönliche Bezeichnung (z.Bsp. Privat, Zu Hause, Hauptadresse)',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );   

        $this->addElement( 'ValidationTextBox', 'address', array(
                            'label'          => 'Strasse Nr.',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );

        $this->addElement( 'ValidationTextBox', 'additional', array(
                            'label'          => 'Zusatz',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );     

         $this->addElement( 'ValidationTextBox', 'zip', array(
                            'label'          => 'Postleitzahl',
                            'regExp'         => '^[0-9]+$',
                            'invalidMessage' => 'Ungültige Zeichen.')
                        );                     

        $this->addElement( 'ValidationTextBox', 'city', array(
                            'label'          => 'Ort',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );       

        $this->addElement( 'CheckBox', 'stopp_kleber', array(
                            'label'          => 'Dieser Briefkasten hat einen Stopp-Kleber',
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             ); 

         $this->addElement( 'ValidationTextBox', 'private_mail', array(
                            'label'          => 'Private E-Mail-Adresse',
                            'required'       => false,
                            'regExp'         => '\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );
          

        $this->addDisplayGroup(array('addresstype','address','additional','zip',
                                     'city','stopp_kleber','private_mail'),
                                     'primary_address', 
                                     array('legend'=>'Privatadresse und private E-Mail'));

        $group = $this->getDisplayGroup('primary_address');                                     
				$group->setAttrib('class','closed');
                                     		
				$this->addElement( 'ValidationTextBox', 'addresstype2', array(
                            'label'          => 'Persönliche Bezeichnung (z.Bsp. Ferienhaus, Dependance)',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );   

        $this->addElement( 'ValidationTextBox', 'address2', array(
                            'label'          => 'Strasse Nr.',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );

        $this->addElement( 'ValidationTextBox', 'additional2', array(
                            'label'          => 'Zusatz',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );             
                        
         $this->addElement( 'ValidationTextBox', 'zip2', array(
                            'label'          => 'Postleitzahl',
                            'regExp'         => '^[0-9]+$',
                            'invalidMessage' => 'Ungültige Zeichen.')
                        );    
                                                
        $this->addElement( 'ValidationTextBox', 'city2', array(
                            'label'          => 'Ort',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );      

        $this->addElement( 'CheckBox', 'stopp_kleber2', array(
                            'label'          => 'Dieser Briefkasten hat einen Stopp-Kleber',
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             );                 

        $this->addDisplayGroup(array('addresstype2','address2','additional2','zip2',
                                     'city2','stopp_kleber2'),
                                     'secondary_address', 
                                     array('legend'=>'Zweitadresse, Ferienhaus'));         

        $group = $this->getDisplayGroup('secondary_address');                                     
				$group->setAttrib('class','closed');		
                                     
        $this->addElement( 'ValidationTextBox', 'addresstype3', array(
                            'label'          => 'Persönliche Bezeichnung (z.Bsp. Geschäft, Job)',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );   
                        
                        
        $this->addElement( 'ValidationTextBox', 'company', array(
                            'label'          => 'Firma',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );  

        $this->addElement( 'ValidationTextBox', 'address3', array(
                            'label'          => 'Strasse Nr.',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );

        $this->addElement( 'ValidationTextBox', 'additional3', array(
                            'label'          => 'Zusatz',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );             

        $this->addElement( 'ValidationTextBox', 'zip3', array(
                            'label'          => 'Postleitzahl',
                            'regExp'         => '^[0-9]+$',
                            'invalidMessage' => 'Ungültige Zeichen.')
                        );  

        $this->addElement( 'ValidationTextBox', 'city3', array(
                            'label'          => 'Ort',
                            'regExp'         => '^[A-Za-z0-9_äÄöÖüÜ \\.\\t]+$',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );  

        $this->addElement( 'CheckBox', 'stopp_kleber3', array(
                            'label'          => 'Dieser Briefkasten hat einen Stopp-Kleber',
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             ); 

				$this->addElement( 'ValidationTextBox', 'email_biz', array(
                            'label'          => 'Geschäftliche E-Mail-Adresse',
                            'regExp'         => '\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );
                        
        $this->addDisplayGroup(array('addresstype3','company','address3','additional3','zip3',
                                     'city3','stopp_kleber3','email_biz'),
                                     'company_address', 
                                     array('legend'=>'Geschäftsadresse'));                                                                                                                  

        $group = $this->getDisplayGroup('company_address');                                     
				$group->setAttrib('class','closed');
				
				$this->addElement( 'SubmitButton', 'register-submit0', array(
                           'required'   => false,
                           'ignore'     => true,
                           'label'      => 'Speichern')
                        );
    }
    
    /** EDUCATION, BUYING POWER, EMPLOYMENT */
    public function educationForm()
    {
    		parent::setName('formUserProfileEducation');
    		                
        $this->addElement( 'RadioButton', 'education', array(
                            'label' => 'Ausbildung',
                            'multiOptions'  => array('keine_Angabe' => 'Keine Angabe',
                            						 'obligatorisch' => 'Obligatorische Schulzeit absolviert',
                                                     'mittel' => 'Weiterführende Schule besucht',
                                                     'hoch' => 'Fachhochschule/Universität besucht'),
                            'value' => 'keine_Angabe')
                        );
                        
        $this->addElement( 'RadioButton', 'degree', array(
                            'label' => 'Abschlüsse',
                            'multiOptions'  => array('keine_Angabe' => 'Keine Angabe',
                            						 'diplom' => 'Lehrabschluss',        
                            						 'matura' => 'Matura',
                                                     'university' => '(Fach-) Hochschulabschluss'),
                            'value' => 'keine_Angabe')
                        );
                        
        $this->addDisplayGroup(array('education','degree'),
                                     'education_degree', 
                                     array('legend'=>'Ausbildung, Berufsabschluss'));
                                     
				$group = $this->getDisplayGroup('education_degree');                                     
				$group->setAttrib('class','semi');                                     
                                     
                                     
         $this->addElement( 'FilteringSelect', 'profession', array(
                            'label'        => 'Berufsfeld',
                            'autocomplete' => false,
                            'multiOptions' => array('choose' => '- Bitte wählen -',
                            						'Family' => 'Familien-Organisation (Hausfrau, Hausmann)',
											         'Industrie' => 'Industrie',
                                                     'Dienstleistungen' => 'Dienstleistungen',
                                                     'Behörde' => 'Behörde',
                                                     'Transport' => 'Transport',
                                                     'Bauwesen' => 'Bauwesen',
                                                     'noValue' => 'Keine Angabe'),
                            )
                        );      

        $this->addElement( 'FilteringSelect', 'businessfunction', array(
                            'label'        => 'Funktion',
                            'autocomplete' => false,
                            'multiOptions' => array( 'choose' => '- Bitte wählen -',
                                                     'Mitarbeiter' => 'Mitarbeiter',
                                                     'Abteilungsleiter' => 'Abteilungsleiter',
                                                     'Führungskraft' => 'Führungskraft',
                                                     'Selbständig' => 'Selbständig',
                                                     'noValue' => 'Keine Angabe'),
                            )
                        );
                        
        $this->addDisplayGroup(array('profession','businessfunction'),
                                     'profession_bizfunction', 
                                     array('legend'=>'Berufsangaben'));
                                     
				$group = $this->getDisplayGroup('profession_bizfunction');                                     
				$group->setAttrib('class','closed');
                        
        $this->addElement( 'HorizontalSlider', 'buyingpower', array(
                            'label'                     => 'Ihre Einschätzung der persönlichen Kaufkraft im schweizerischen Vergleich',
                            'minimum'                   => 0,
                            'maximum'                   => 10,
                            'discreteValues'            => 11,
                            'intermediateChanges'       => true,
                            'showButtons'               => false,
                            'style'											=> 'width: 350px;',
                            'topDecorationDijit'        => 'HorizontalRuleLabels',
                            'topDecorationContainer'    => 'topContainer',
                            'topDecorationLabels'       => array(' ','20%','40%','60%','80%',' '),
                            'topDecorationParams'      => array(
                                'container' => array('style' => 'height:1.2em; font-size=11px;color:gray;'),
                                'list' => array('style' => 'height:1em; font-size=11px;color:gray;'),
                                    ),
                            'bottomDecorationDijit'     => 'HorizontalRule',
                            'bottomDecorationContainer' => 'bottomContainer',
                            'bottomDecorationLabels'    => array('niedrig','mittel','hoch'),
                            'bottomDecorationParams'   => array(
                                'list' => array('style' => 'height:1.2em; font-size=5px;color:gray;'),
                                    ),
                            )
                        );      
                        
        $this->addElement( 'HorizontalSlider', 'free_income', array(
                            'label'                     => 'Prozentsatz des frei verfügbaren Einkommens',
                            'minimum'                   => -10,
                            'maximum'                   => 10,
                            'discreteValues'            => 11,
                            'intermediateChanges'       => true,
                            'showButtons'               => false,
                            'style'											=> 'width: 350px;',
                            'topDecorationDijit'        => 'HorizontalRuleLabels',
                            'topDecorationContainer'    => 'topContainer',
                            'topDecorationLabels'       => array(' ','20%','40%','60%','80%',' '),
                            'topDecorationParams'      => array(
                                'container' => array('style' => 'height:1.2em; font-size=11px;color:gray;'),
                                'list' => array('style' => 'height:1em; font-size=11px;color:gray;'),
                                    ),
                            'bottomDecorationDijit'     => 'HorizontalRule',
                            'bottomDecorationContainer' => 'bottomContainer',
                            'bottomDecorationLabels'    => array('wenig','viel','alles'),
                            'bottomDecorationParams'   => array(
                                'list' => array('style' => 'height:1.2em; font-size=5px;color:gray;'),
                                    ),
                            )
                        ); 

        $this->addDisplayGroup(array('buyingpower','free_income'),
                                     'buying_power', 
                                     array('legend'=>'Einschätzung der Kaufkraft und des frei verfügbaren Einkommens'));
                                     
				$group = $this->getDisplayGroup('buying_power');                                     
				$group->setAttrib('class','open');                                     


         $this->addElement( 'CheckBox', 'familymanager', array(
                            'label'          => 'Ich bin Familienmanager',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        ); 
                                     
         $this->addElement( 'CheckBox', 'decisionmaker', array(
                            'label'          => 'Ich bin Entscheidungsträger (geschäftlich)',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        );                    
                        
         $this->addElement( 'CheckBox', 'vrmandat', array(
                            'label'          => 'Ich habe VR-Mandate',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        );

        $this->addDisplayGroup(array('familymanager','decisionmaker','vrmandat'),
                                     'fammanager_decisionmaker_vr', 
                                     array('legend'=>'Stellung als Familienmanager, Entscheidungsträger'));            

            $group = $this->getDisplayGroup('fammanager_decisionmaker_vr');                                     
			$group->setAttrib('class','open');                        
                                     
                                     
          $this->addElement( 'CheckBox', 'homeoffice', array(
                            'label'          => 'Gut ausgerüstetes Home-Office',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        );  

            $this->addElement( 'CheckBox', 'homework', array(
                            'label'          => 'Arbeite regelmässig von zu Hause aus',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        ); 
                                     
        $this->addDisplayGroup(array('homeoffice','homework'),
                                     'homeoffice_homework', 
                                     array('legend'=>'Homeoffice und Telearbeit')); 

            $group = $this->getDisplayGroup('homeoffice_homework');                                     
			$group->setAttrib('class','open');
                                     
            $this->addElement( 'SubmitButton', 'register-submit1', array(
                           'required'   => false,
                           'ignore'     => true,
                           'label'      => 'Speichern')
                        ); 
                                     
    }   
    
    /** HOUSEHOLD */                                     
    public function householdForm()
    {     
    		parent::setName('formUserProfileHousehold');
    		                                 
        $this->addElement( 'FilteringSelect', 'householdtype', array(
                            'label'        => 'Haushaltstyp',
                            'autocomplete' => false,
                            'multiOptions' => array( 'choose' => '- Bitte wählen -',
                            						 						 'Single' => 'Single',
                                                     'Konkubinat' => 'Konkubinat',
                                                     'Familie' => 'Familie',
                                                     'WG' => 'WG',
        											'noValue' => 'Keine Angabe'),
                            )
                        );                             
                                     
        $this->addElement( 'NumberSpinner', 'householdsize', array(
                    'label'             => 'Haushaltsgrösse (Anzahl Personen)',
                    'smallDelta'        => 1,
                    'largeDelta'        => 25,
                    'defaultTimeout'    => 1000,
                    'timeoutChangeRate' => 100,
                    'min'               => 1,
                    'max'               => 20,
                    'places'            => 0,
                    'maxlength'         => 20,
                )
            );                  
                                            
        $this->addDisplayGroup(array('householdtype','householdsize'),'household', array('legend'=>'Haushalt'));     

        $group = $this->getDisplayGroup('household');                                     
			  $group->setAttrib('class','open');
        
        $this->addElement( 'NumberSpinner', 'children', array(
                    'label'             => 'Kinder (total)',
                    'smallDelta'        => 1,
                    'largeDelta'        => 25,
                    'defaultTimeout'    => 1000,
                    'timeoutChangeRate' => 100,
                    'min'               => 0,
                    'max'               => 20,
                    'places'            => 0,
                    'maxlength'         => 20,
                )
            );

         $this->addElement( 'NumberSpinner', 'children_preschool', array(
                    'label'             => 'Kinder im Vorschulalter',
                    'smallDelta'        => 1,
                    'largeDelta'        => 25,
                    'defaultTimeout'    => 1000,
                    'timeoutChangeRate' => 100,
                    'min'               => 0,
                    'max'               => 20,
                    'places'            => 0,
                    'maxlength'         => 20,
                )
            );

        $this->addElement( 'NumberSpinner', 'children_standardschool', array(
                    'label'             => 'Kinder im oblig. Schulalter',
                    'smallDelta'        => 1,
                    'largeDelta'        => 25,
                    'defaultTimeout'    => 1000,
                    'timeoutChangeRate' => 100,
                    'min'               => 0,
                    'max'               => 20,
                    'places'            => 0,
                    'maxlength'         => 20,
                )
            );   

        $this->addElement( 'NumberSpinner', 'children_advancedschool', array(
                    'label'             => 'Kinder im Zusatzausbildung, inkl. Lehre',
                    'smallDelta'        => 1,
                    'largeDelta'        => 25,
                    'defaultTimeout'    => 1000,
                    'timeoutChangeRate' => 100,
                    'min'               => 0,
                    'max'               => 20,
                    'places'            => 0,
                    'maxlength'         => 20,
                )
            );
        
         $this->addDisplayGroup(array('children','children_preschool','children_standardschool','children_advancedschool'),'children_details', array('legend'=>'Kinder im Haushalt'));  

         $group = $this->getDisplayGroup('children_details');                                     
			   $group->setAttrib('class','semi');
       
         $this->addElement( 'CheckBox', 'pet_dog', array(
                            'label'          => 'Hund(e) im Haus',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             );   
             
         $this->addElement( 'CheckBox', 'pet_cat', array(
                            'label'          => 'Katze(n) im Haus',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             ); 
             
         $this->addElement( 'CheckBox', 'pet_small', array(
                            'label'          => 'Vogel oder andere Kleintiere',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             ); 
             
         $this->addElement( 'CheckBox', 'pet_large', array(
                            'label'          => 'Pferd oder andere Grosstiere',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             ); 
       
       
         $this->addDisplayGroup(array('pet_dog','pet_cat','pet_small','pet_large'),'pet', array('legend'=>'Haustiere'));   

         $group = $this->getDisplayGroup('pet');                                     
			   $group->setAttrib('class','open');

         $this->addElement( 'FilteringSelect', 'areatype', array(
                            'label'        => 'Wohngebietstyp',                            
                            'autocomplete' => false,
                            'multiOptions' => array( 'choose' => '- Bitte wählen -',
                            						 						 'upper_class' => 'Upper Class',
                                                     'conservative' => 'Konservativ',
                                                     'middle' => 'Gehobene Mitte',
                                                     'traditional_worker' => 'Trad. Arbeiter',
        											 											 'farm' => 'Ländlich, Bauernhof',
                                                     'frige_group' => 'Randgruppen',
        											 											 'noValue' => 'Keine Angabe')
                            )
                        );
                        
         $this->addElement( 'FilteringSelect', 'house_size', array(
                            'label'        => 'Wohnsituation',
                            'autocomplete' => false,
                            'multiOptions' => array( 'choose' => '- Bitte wählen -',
                            						 						 '1_2_fh' => '1-2 Familienhaus',
                                                     '3_5_fh' => '3-5 Familienhaus',
                                                     '6_10_fh' => '6-10 Familienhaus',
                                                     'over_10_fh' => 'über 10 Familienhaus',
        											 											 'noValue' => 'Keine Angabe'),
                            )
                        );
        
        $this->addElement( 'NumberSpinner', 'squaremeters', array(
                    'label'             => 'Wohnfläche in Quadratmeter',
                    'smallDelta'        => 1,
                    'largeDelta'        => 25,
                    'defaultTimeout'    => 1000,
                    'timeoutChangeRate' => 100,
                    'min'               => 10,
                    'max'               => 400,
                    'places'            => 0,
                    'maxlength'         => 20,
                )
            ); 
            
            $this->addElement( 'CheckBox', 'ownership', array(
                            'label'          => 'Eigentümer',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             );
             
             $this->addElement( 'ValidationTextBox', 'build_year', array(
                            'label'          => 'Baujahr (z.Bsp. 1980)',
                            'required'       => false,
                            'regExp'         => '[\w]+',
                            'invalidMessage' => 'Ungültige Buchstaben.')
                        );
                        
             $this->addElement( 'FilteringSelect', 'main_energy', array(
                            'label'        => 'Dominierender Energieträger',
                            'autocomplete' => false,
                            'multiOptions' => array( 'choose' => '- Bitte wählen -',
                            						 						 'oil' => 'Heizöl',
                                                     'gas' => 'Gas',
                                                     'electric' => 'Elektro',
                                                     'wood' => 'Holz',
                                                     'sun' => 'Sonne',
                                                     'earth_heat' => 'Erdwärme',
                                                     'remote_heat' => 'Fernwärme',
        											 											 'noValue' => 'Keine Angabe'),
                            )
                        );
        
        $this->addDisplayGroup(array('areatype','house_size','squaremeters','ownership','build_year','main_energy'),'living_situation', array('legend'=>'Wohnsituation'));                                                                       
        
                    $group = $this->getDisplayGroup('living_situation');                                     
			$group->setAttrib('class','open');
        
        $this->addElement( 'SubmitButton', 'register-submit2', array(
                           'required'   => false,
                           'ignore'     => true,
                           'label'      => 'Speichern')
                        ); 
    }
      
    /** CONSUME */                                     
    public function consumeForm()
    {
    			parent::setName('formUserProfileConsume');
    	
        	$this->addElement( 'HorizontalSlider', 'mailorder', array(
                            'label'                     => 'Postkaufaffinität: Kaufen Sie per Post-Bestellung ein?',
                            'minimum'                   => -10,
                            'maximum'                   => 10,
                            'discreteValues'            => 11,
                            'intermediateChanges'       => true,
                            'showButtons'               => false,
                            'style'											=> 'width: 350px;',
                            'topDecorationDijit'        => 'HorizontalRuleLabels',
                            'topDecorationContainer'    => 'topContainer',
                            'topDecorationLabels'       => array(' ','20%','40%','60%','80%',' '),
                            'topDecorationParams'      => array(
                                'container' => array('style' => 'height:1.2em; font-size=11px;color:gray;'),
                                'list' => array('style' => 'height:1em; font-size=11px;color:gray;'),
                                    ),
                            'bottomDecorationDijit'     => 'HorizontalRule',
                            'bottomDecorationContainer' => 'bottomContainer',
                            'bottomDecorationLabels'    => array('nie','regelmässig','oft'),
                            'bottomDecorationParams'   => array(
                                'list' => array('style' => 'height:1.2em; font-size=5px;color:gray;'),
                                    ),
                            )
                        );

                 $this->addElement( 'HorizontalSlider', 'online_shopping', array(
                            'label'                     => 'Online-Kaufaffinität: Kaufen Sie online ein?',
                            'value'                     => 5,
                            'minimum'                   => -10,
                            'maximum'                   => 10,
                            'discreteValues'            => 11,
                            'intermediateChanges'       => true,
                            'showButtons'               => false,
                            'style'											=> 'width: 350px;',
                            'topDecorationDijit'        => 'HorizontalRuleLabels',
                            'topDecorationContainer'    => 'topContainer',
                            'topDecorationLabels'       => array(' ','20%','40%','60%','80%',' '),
                            'topDecorationParams'      => array(
                                'container' => array('style' => 'height:1.2em; font-size=11px;color:gray;'),
                                'list' => array('style' => 'height:1em; font-size=11px;color:gray;'),
                                    ),
                            'bottomDecorationDijit'     => 'HorizontalRule',
                            'bottomDecorationContainer' => 'bottomContainer',
                            'bottomDecorationLabels'    => array('nie','regelmässig','oft'),
                            'bottomDecorationParams'   => array(
                                'list' => array('style' => 'height:1.2em; font-size=5px;color:gray;'),
                                    ),
                            )
                        );
                        
                 $this->addElement( 'HorizontalSlider', 'donating', array(
                            'label'                     => 'Spender-Affinität: Spenden Sie für wohltätige Zwecke?',
                            'value'                     => 5,
                            'minimum'                   => -10,
                            'maximum'                   => 10,
                            'discreteValues'            => 11,
                            'intermediateChanges'       => true,
                            'showButtons'               => false,
                            'style'											=> 'width: 350px;',
                            'topDecorationDijit'        => 'HorizontalRuleLabels',
                            'topDecorationContainer'    => 'topContainer',
                            'topDecorationLabels'       => array(' ','20%','40%','60%','80%',' '),
                            'topDecorationParams'      => array(
                                'container' => array('style' => 'height:1.2em; font-size=11px;color:gray;'),
                                'list' => array('style' => 'height:1em; font-size=11px;color:gray;'),
                                    ),
                            'bottomDecorationDijit'     => 'HorizontalRule',
                            'bottomDecorationContainer' => 'bottomContainer',
                            'bottomDecorationLabels'    => array('nie','regelmässig','oft'),
                            'bottomDecorationParams'   => array(
                                'list' => array('style' => 'height:1.2em; font-size=5px;color:gray;'),
                                    ),
                            )
                        );
                        
             $this->addDisplayGroup(array('mailorder','online_shopping','donating'),
             'behavioral_characteristics', array('legend'=>'Ihr Einkaufs- und Spendenverhalten'));       

            $group = $this->getDisplayGroup('behavioral_characteristics');                                     
			$group->setAttrib('class','open');
             
             $this->addElement( 'CheckBox', 'hunter', array(
                            'label'          => 'Ich bin ein Schnäppchenjäger',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             );   
             
            $this->addElement( 'CheckBox', 'careful', array(
                            'label'          => 'Ich wäge Preis-Leistung sorgfältig ab',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             ); 
             
             $this->addElement( 'CheckBox', 'spontaneous', array(
                            'label'          => 'Ich kaufe eher spontan ein',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             ); 
             
             $this->addElement( 'CheckBox', 'quality', array(
                            'label'          => 'Ich setze vorallem auf gute Qualität',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             ); 
       
       
	       $this->addDisplayGroup(array('hunter','careful','spontaneous','quality'),
	       'shopping_type', array('legend'=>'Einkaufstyp: Als welchen Typ würden Sie sich bezeichnen?'));   

	        $group = $this->getDisplayGroup('shopping_type');                                     
			$group->setAttrib('class','open');
	       
	        $this->addElement( 'CheckBox', 'coop_migros', array(
                            'label'          => 'Coop und/oder Migros (auch online)',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             );   
             
            $this->addElement( 'CheckBox', 'aldi_lidl', array(
                            'label'          => 'Aldi und/oder Lidl',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             ); 
             
             $this->addElement( 'CheckBox', 'village_farm', array(
                            'label'          => 'Dorfladen, Landi oder Bauernhof',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             ); 
             
             $this->addElement( 'CheckBox', 'special', array(
                            'label'          => 'Spezialitätengeschäfte',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
             ); 
       
       
	       $this->addDisplayGroup(array('coop_migros','aldi_lidl','village_farm','special'),
	       'shopping_daily', array('legend'=>'Wo kaufen Sie für den täglichen Gebrauch ein?')); 

	        $group = $this->getDisplayGroup('shopping_daily');                                     
			$group->setAttrib('class','open');
	       
	       $this->addElement( 'HorizontalSlider', 'tv', array(
                            'label'                     => 'Durchschnittlicher TV-Konsum pro Tag',
                            'value'                     => 5,
                            'minimum'                   => -10,
                            'maximum'                   => 10,
                            'discreteValues'            => 11,
                            'intermediateChanges'       => true,
                            'showButtons'               => false,
                            'style'											=> 'width: 350px;',
                            'topDecorationDijit'        => 'HorizontalRuleLabels',
                            'topDecorationContainer'    => 'topContainer',
                            'topDecorationLabels'       => array(' ','2h','4h','6h','8h',' '),
                            'topDecorationParams'      => array(
                                'container' => array('style' => 'height:1.2em; font-size=11px;color:gray;'),
                                'list' => array('style' => 'height:1em; font-size=11px;color:gray;'),
                                    ),
                            'bottomDecorationDijit'     => 'HorizontalRule',
                            'bottomDecorationContainer' => 'bottomContainer',
                            'bottomDecorationLabels'    => array('wenig','viel','dauernd'),
                            'bottomDecorationParams'   => array(
                                'list' => array('style' => 'height:1.2em; font-size=5px;color:gray;'),
                                    ),
                            )
                        );

                 $this->addElement( 'HorizontalSlider', 'radio', array(
                            'label'                     => 'Durchschnittlicher Radio-Konsum pro Tag',
                            'value'                     => 5,
                            'minimum'                   => -10,
                            'maximum'                   => 10,
                            'discreteValues'            => 11,
                            'intermediateChanges'       => true,
                            'showButtons'               => false,
                            'style'											=> 'width: 350px;',
                            'topDecorationDijit'        => 'HorizontalRuleLabels',
                            'topDecorationContainer'    => 'topContainer',
                            'topDecorationLabels'       => array(' ','2h','4h','6h','8h',' '),
                            'topDecorationParams'      => array(
                                'container' => array('style' => 'height:1.2em; font-size=11px;color:gray;'),
                                'list' => array('style' => 'height:1em; font-size=11px;color:gray;'),
                                    ),
                            'bottomDecorationDijit'     => 'HorizontalRule',
                            'bottomDecorationContainer' => 'bottomContainer',
                            'bottomDecorationLabels'    => array('wenig','viel','dauernd'),
                            'bottomDecorationParams'   => array(
                                'list' => array('style' => 'height:1.2em; font-size=5px;color:gray;'),
                                    ),
                            )
                        );
                        
                 $this->addElement( 'HorizontalSlider', 'internet', array(
                            'label'                     => 'Durchschnittliche Internet-Nutzung pro Tag (nur zum Vergnügen)',
                            'value'                     => 5,
                            'minimum'                   => -10,
                            'maximum'                   => 10,
                            'discreteValues'            => 11,
                            'intermediateChanges'       => true,
                            'showButtons'               => false,
                            'style'											=> 'width: 350px;',
                            'topDecorationDijit'        => 'HorizontalRuleLabels',
                            'topDecorationContainer'    => 'topContainer',
                            'topDecorationLabels'       => array(' ','2h','4h','6h','8h',' '),
                            'topDecorationParams'      => array(
                                'container' => array('style' => 'height:1.2em; font-size=11px;color:gray;'),
                                'list' => array('style' => 'height:1em; font-size=11px;color:gray;'),
                                    ),
                            'bottomDecorationDijit'     => 'HorizontalRule',
                            'bottomDecorationContainer' => 'bottomContainer',
                            'bottomDecorationLabels'    => array('wenig','viel','dauernd'),
                            'bottomDecorationParams'   => array(
                                'list' => array('style' => 'height:1.2em; font-size=5px;color:gray;'),
                                    ),
                            )
                        );
                        
                        
              $this->addElement( 'HorizontalSlider', 'games', array(
                            'label'                     => 'Durchschnittliche Game-Time pro Tag',
                            'value'                     => 5,
                            'minimum'                   => -10,
                            'maximum'                   => 10,
                            'discreteValues'            => 11,
                            'intermediateChanges'       => true,
                            'showButtons'               => false,
                            'style'											=> 'width: 350px;',
                            'topDecorationDijit'        => 'HorizontalRuleLabels',
                            'topDecorationContainer'    => 'topContainer',
                            'topDecorationLabels'       => array(' ','2h','4h','6h','8h',' '),
                            'topDecorationParams'      => array(
                                'container' => array('style' => 'height:1.2em; font-size=11px;color:gray;'),
                                'list' => array('style' => 'height:1em; font-size=11px;color:gray;'),
                                    ),
                            'bottomDecorationDijit'     => 'HorizontalRule',
                            'bottomDecorationContainer' => 'bottomContainer',
                            'bottomDecorationLabels'    => array('wenig','viel','dauernd'),
                            'bottomDecorationParams'   => array(
                                'list' => array('style' => 'height:1.2em; font-size=5px;color:gray;'),
                                    ),
                            )
                        );
                        
             $this->addDisplayGroup(array('tv','radio','internet','games'),
             'media_use', array('legend'=>'Ihr Medienverhalten (Wert 5 GOPS)'));   
        
                    $group = $this->getDisplayGroup('media_use');                                     
			$group->setAttrib('class','open');
        
        $this->addElement( 'SubmitButton', 'register-submit3', array(
                           'required'   => false,
                           'ignore'     => true,
                           'label'      => 'Speichern')
                        ); 
                        
    }
    
    /** MOBILITY */                                     
    public function mobilityForm()
    {
    	
    	parent::setName('formUserProfileMobility');
        
    	$this->addElement( 'FilteringSelect', 'primary_vehicle', array(
                            'label'        => 'Primäres Mobilitätsmittel',
                            'value'        => 'blue',
                            'autocomplete' => false,
                            'multiOptions' => array( 'choose' => '- Bitte wählen -',
                                                     'public_transportation' => 'Zu Fuss, öffentliche Verkehrsmittel',
                                                     'bike_mofa' => 'Velo, Moped',
                                                     'motorbike' => 'Motorrad',
                                                     'car' => 'Auto',
                                                     'noValue' => 'Keine Angabe'),
                            )
                        );
                        
                        $this->addElement( 'HorizontalSlider', 'vehicle_class', array(
                            'label'                     => 'Fahrzeugklasse Auto oder Motorrad',
                            'value'                     => 5,
                            'minimum'                   => -10,
                            'maximum'                   => 10,
                            'discreteValues'            => 11,
                            'intermediateChanges'       => true,
                            'showButtons'               => false,
                            'style'											=> 'width: 350px;',
                            'topDecorationDijit'        => 'HorizontalRuleLabels',
                            'topDecorationContainer'    => 'topContainer',
                            'topDecorationLabels'       => array(' ','20%','40%','60%','80%',' '),
                            'topDecorationParams'      => array(
                                'container' => array('style' => 'height:1.2em; font-size=11px;color:gray;'),
                                'list' => array('style' => 'height:1em; font-size=11px;color:gray;'),
                                    ),
                            'bottomDecorationDijit'     => 'HorizontalRule',
                            'bottomDecorationContainer' => 'bottomContainer',
                            'bottomDecorationLabels'    => array('niedrig','mittel','hoch'),
                            'bottomDecorationParams'   => array(
                                'list' => array('style' => 'height:1.2em; font-size=5px;color:gray;'),
                                    ),
                            )
                        );
                        
                        $this->addElement( 'CheckBox', 'commuter', array(
                            'label'          => 'Pendler',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        ); 
    	
    	$this->addDisplayGroup(array('primary_vehicle','vehicle_class','commuter'),'mobility_characteristics', array('legend'=>'Mobilitätsmerkmale'));                                                                       
        
    	            $group = $this->getDisplayGroup('mobility_characteristics');                                     
			$group->setAttrib('class','open');
        
        
        $this->addElement( 'SubmitButton', 'register-submit4', array(
                           'required'   => false,
                           'ignore'     => true,
                           'label'      => 'Speichern')
                        ); 
    	
    	
    }
    
    /** LIFESTYLE */                                     
    public function lifestyleForm()
    {
      parent::setName('formUserProfileLifestyle');  
    	
    	$this->addElement( 'CheckBox', 'professional', array(
                            'label'          => 'Wirtschaftsprofi',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        ); 
                        
        $this->addElement( 'CheckBox', 'powerwoman', array(
                            'label'          => 'Powerfrau',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        );
                        
        $this->addElement( 'CheckBox', 'rooted', array(
                            'label'          => 'Bodenständig',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        );
                        
        $this->addElement( 'CheckBox', 'creative', array(
                            'label'          => 'Kreativ',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        );
    	
         $this->addElement( 'CheckBox', 'dink', array(
                            'label'          => 'DINK (double income, no kids)',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        ); 

         $this->addElement( 'CheckBox', 'lohas', array(
                            'label'          => 'LOHAS (Lifestyle of Health and Sustainability)',
                            'required'			 => true,
                            'checkedValue'   => 'yes',
                            'uncheckedValue' => 'no',
                            'checked'        => false)
                        );
    	
    	$this->addDisplayGroup(array('professional','powerwoman','rooted','creative','dink','lohas'),'lifestyle_type', array('legend'=>'Lifestyle-Typ'));    

    	            $group = $this->getDisplayGroup('lifestyle_type');                                     
			$group->setAttrib('class','open');
    	
    	$this->addElement( 'RadioButton', 'psychografic', array(
                            'label' => 'Psychografische Nähe wählen',
                            'multiOptions'  => array('novalue' => 'Weiss nicht',
    												'arrivierte' => 'Arrivierte',
                                                     'postmateriell' => 'Postmaterielle',
    												 'modern_performer' => 'Moderne Perfomer',
     												 'traditional' => 'Genügsame, Traditionelle',
    	    										 'status_oriented' => 'Statusorientiert',
    	    										 'buergerliche_mitte' => 'Bürgerliche Mitte',
    												 'consumoriented_worker' => 'Konsumorientierte Arbeiter',
    	   	                                         'experimentalist' => 'Experimentalisten'),
                            'value' => 'novalue')
                        );
    	
    	$this->addDisplayGroup(array('psychografic'),'psychografic_characteristics', array('legend'=>'Psychografische Merkmale'));   
        
                    $group = $this->getDisplayGroup('psychografic_characteristics');                                     
			$group->setAttrib('class','open');
        
        $this->addElement( 'SubmitButton', 'register-submit5', array(
                           'required'   => false,
                           'ignore'     => true,
                           'label'      => 'Speichern')
                        ); 
                        
                        
                        
                        
    	
    }
}

